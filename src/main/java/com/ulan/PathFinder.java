package com.ulan;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Jiona
 * Date: 22.02.14
 * Time: 6:06
 * To change this template use File | Settings | File Templates.
 */
public class PathFinder {

    private final Labyrinth labyrinth;
    private final Point startPoint;
    

    public PathFinder(Labyrinth labyrinth, Point startPoint) {
        this.labyrinth = labyrinth;
        this.startPoint = startPoint;
    }


    /**
     * Finds path from start point to end point or return null if there is no path
     *
     * @param endPoint   the end point
     * @return the path or null if path doesn't exists
     */
    public List<Point> findPath(Point endPoint) {
        
        int height = labyrinth.getHeight();
        int width = labyrinth.getWidth();
        int i = startPoint.getI();
        int j = startPoint.getJ();
        int endPointI = endPoint.getI();
        int endPointJ = endPoint.getJ();

        int [][] ints = new int[height][width];
        for (int x=0; x<labyrinth.getHeight(); x++){
            for (int y=0; y<labyrinth.getWidth(); y++) {
            if (labyrinth.isRoom(x,y)) ints[x][y]=-3;
            else ints[x][y]=0;
            }
        }

        int iter = 0;
        ints[i][j]=0;//обманка, что бы не проверять стартовую точку
        bloom(i, j, iter, ints);
        iter++;
        
        while (iter!=-1 && iter!=-2) {
            iter = checkWave(iter, ints, endPointI, endPointJ);
        }
        if (iter==-1)
            {System.out.println("We find the path");
            i=endPointI;j=endPointJ;

            List<Point> thePath = new ArrayList<>();
            for (int listSize=0; listSize<=ints[endPointI][endPointJ]; listSize++){thePath.add(null);}//
                for (int length=ints[endPointI][endPointJ]; length>=0; length--){
                    Point point = new Point(i,j);
                    thePath.set(length,point);
                    if (length==1) {
                        point = new Point(startPoint.getI(),startPoint.getJ());
                        thePath.set(0,point);
                        length--;
                    } else {
                    if ((j + 1) < width && ints[i][j+1] == length - 1) {
                        j++;
                    } else {if ((i + 1) < height && ints[i+1][j] == length - 1) {
                            i++;
                            } else {if (j != 0 && ints[i][j-1] == length - 1) {
                                    j--;
                                    } else {if (i != 0 && ints[i - 1][j] == length - 1) {
                                            i--;
                                        }}}}}
                }
                return thePath;
            }
        if (iter==-2)
            {System.out.println("The path is not exists");
            return null;}
        return null;
    }

    /**
     * Checks whether path from start point to end point exists
     *
     * @param endPoint   the end point
     * @return flag is path exists or not
     */
    public boolean isPathExists(Point endPoint) {
        int [][] ints = new int[labyrinth.getHeight()][labyrinth.getWidth()];
        for (int x=0; x<labyrinth.getHeight(); x++){
            for (int y=0; y<labyrinth.getWidth(); y++) {
                if (labyrinth.isRoom(x,y)) ints[x][y]=-3;
                else ints[x][y]=0;
            }
        }
        int iter = 0;
        ints[startPoint.getI()][startPoint.getJ()]=0;//обманка, что бы не проверять стартовую точку
        bloom(startPoint.getI(), startPoint.getJ(), iter, ints);
        iter++;

        while (iter!=-1 && iter!=-2) {
            iter = checkWave(iter, ints, endPoint.getI(), endPoint.getJ());
        }
        if (iter==-1)
        {System.out.println("We find the path");
            return true;
        }
        if (iter==-2)
        {System.out.println("The path is not exists");
            return false;}
        return false;
    }


    public int checkWave(int iter, int[][] ints, int endPointI, int endPointJ){
        int count=0;
        for (int x=0; x<labyrinth.getHeight(); x++){
            for (int y=0; y<labyrinth.getWidth(); y++){
                if (ints[x][y]==iter) {
                    if (x==endPointI && y==endPointJ)
                    {return iter=-1;}
                        else
                    {bloom(x,y,iter,ints);
                    count++;} 
                }
            }

        }
        if (count==0)
            {iter=-2;
            return iter;}
                else
        return ++iter;
    }

    public void bloom(int i, int j, int iter, int[][] ints) {
        if (((j + 1) < labyrinth.getWidth()) && (ints[i][j+1] != iter - 1) && (ints[i][j+1] != 0)) {
            ints[i][j+1] = iter + 1;
        }
        if ((i + 1) < labyrinth.getHeight() && ints[i+1][j] != iter - 1 && ints[i+1][j] != 0) {
            ints[i+1][j] = iter + 1;
        }
        if (j != 0 && ints[i][j-1] != iter - 1 && ints[i][j-1] != 0) {
            ints[i][j-1] = iter + 1;
        }
        if (i != 0 && ints[i-1][j] != iter - 1 && ints[i-1][j] != 0) {
            ints[i-1][j] = iter + 1;
        }
    }

}
